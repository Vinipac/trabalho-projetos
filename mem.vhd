-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
-- UFPR, BCC, ci210 2018-2 trabalho semestral, autor: Roberto Hexsel, 31out
-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

use work.p_wires.all;

entity mem_prog is
  port (ender : in  reg7;
        instr : out reg32);

  type t_prog_mem is array (0 to 127) of reg32;

  -- memoria de programa contem somente 64 palavras
  constant program : t_prog_mem := (
    x"c00f0004", --	jal init 				0x00				jal init									$v0 = 1   
    x"c00f0013", --	jal diag				0x01 				jal diag									x"1234000" -> 1 = instrucao 2 = R(a) 3 = R(b) 4 = R(c) 
    x"b1000000", -- show 1		       		0x02				show $v0							   		mostra valor da soma
    x"f0000000", -- halt					0x03				halt										$ra = 15
    x"80040000", -- $r4 <= 0                0x04				init:	addi $r4,$r0,0						$r4 = 4 -> $r4 = 0 row counter
    x"8005000f", -- $r5 <= 16               0x05  				addi $r5,$r0,16								$r5 = 5 -> $r5 = 16 row number
    x"10fd0000", --	$r13 <= $ra				0x06				add  $r13,$r0,$ra							salva endereço de retorno
    x"e4500011", --	i($r4) == 16 -> fim1	0x07				while:  bran $r4,$r5,fim1					$r6 = 6 -> $r6 = 0 column counter
    x"80060000", --	$r6 <= 0				0x08 				addi $r6,$r0,0								$r7 = 7 -> $r7 = i + j Value to put
    x"e650000f", --	j($r6) == 16 -> fim2	0x09				loop:	bran $r6,$r5,fim2					$r8 =  8 -> offset de byte para memoria
    x"14670000", -- $r7 <= i + j			0x0a				add  $r7,$r4,$r6							$r9 = 9 -> recebe valor da diagonal
    x"34580000", --	$r8 <= i * 16			0x0b				mult $r8,$r4,$r5							$r13 = 13 -> $r13 = $ra para retorno
    x"18680000", --	$r8	<= $r8 + (desloc)	0x0c				add  $r8,$r8,$r6							$r8 = deslocamento 
    x"a8700000", --	M($r8)	<= $r7			0x0d				st	 $r7,0($r8)								memoria $r8 < recebe $r7
    x"c00f0009", --	jal loop				0x0e				jal  loop									pula para lopp
    x"84040001", --	$r4 <= $r4 + 1			0x0f				fim2:	addi $r4,$r4,1						$r4 = $r4 + 1 -> row counter + 1	
    x"c00f0007", --	jal while				0x10 				jal  while 									pula para while
    x"10df0000", -- $ra <= $r13				0x11 				fim1:	add  $ra,$r0,$r13					$ra recebe endereço de retorno guardado
    x"df000000", -- jr $ra					0x12 				jr 	 $ra									pula para o endereço no registrador 
    x"90040000", -- $r4 <= 0                0x13				diag:   addi $r4,$r0,0						$r4 = 4 -> $r4 = 0 row counter
    x"90010000", -- $v0 <= 0				0x14 				addi $v0,$r0,0								$v0 = 0 -> zera o valor de retorno da função
    x"10fd0000", -- $r13 <= $ra				0x15 				add  $r13,$r0,$ra							$r13 = $ra salva enredeço de retorno
    x"e450001c", --	i($r4) == 16 -> fim3	0x16 				while2: bran $r4,$r5,fim3					se $r4 = $r5 vai para fim3 -> row counter = 16
    x"34580000", -- $r8 <= i * 16			0x17 				mult $r8,$r4,$r5							$r8 = i * 16 -> offset para a linha
    x"18480000", -- $r8 <= $r8 + (desloc)	0x18 				add  $r8,$r8,$r4							$r8 = $r8 + j -> offset qual coluna 
    x"98090000", --	$r9 <= M($r8)			0x19 				ld   $r9,0($r8)								$r9 <= M($r8)	
    x"11910000", -- $v0 <= $v0 + $r9		0x1a 				add  $v0,$v0,$r9							$v0 = $v0 + $r9
    x"c00f0016", --	jal while2				0x1b 				jal  while2									pula para while2	
    x"10df0000", -- $ra <= $r13				0x1c				fim3:	add  $ra,$r0,$r13					$ra = $r13 endereço de retorno guaradado		
    x"df000000", -- jr $ra					0x1d				jr   $ra									pula para o registrador de retorno
    x"00000000",
    x"00000000",
    x"00000000",
    x"00000000",
    x"00000000",
    x"00000000",
    x"00000000",
    x"00000000",
    x"00000000",
    x"00000000",
    x"00000000",
    x"00000000",
    x"00000000",
    x"00000000",
    x"00000000",
    x"00000000",
    x"00000000",
    x"00000000",
    x"00000000",
    x"00000000",
    x"00000000",
    x"00000000",
    x"00000000",

    x"00000000",
    x"00000000",
    x"00000000",
    x"00000000",
    x"00000000",
    x"00000000",
    x"00000000",
    x"00000000",
    x"00000000",
    x"00000000",
    x"00000000",
    x"00000000",
    x"00000000",
    x"00000000",
    x"00000000",
    x"00000000",
    x"00000000",
    x"00000000",
    x"00000000",
    x"00000000",
    x"00000000",
    x"00000000",
    x"00000000",
    x"00000000",
    x"00000000",
    x"00000000",
    x"00000000",
    x"00000000",
    x"00000000",
    x"00000000",
    x"00000000",
    x"00000000",

    x"00000000",
    x"00000000",
    x"00000000",
    x"00000000",
    x"00000000",
    x"00000000",
    x"00000000",
    x"00000000",
    x"00000000",
    x"00000000",
    x"00000000",
    x"00000000",
    x"00000000",
    x"00000000",
    x"00000000",
    x"00000000",
    x"00000000",
    x"00000000",
    x"00000000",
    x"00000000",
    x"00000000",
    x"00000000",
    x"00000000",
    x"00000000",
    x"00000000",
    x"00000000",
    x"00000000",
    x"00000000",
    x"00000000",
    x"00000000",
    x"00000000",
    x"00000000"
  );


  function BV2INT7(S: reg7) return integer is
    variable result: integer;
  begin
    for i in S'range loop
      result := result * 2;
      if S(i) = '1' then
        result := result + 1;
      end if;
    end loop;
    return result;
  end BV2INT7;
  
end mem_prog;

-- nao altere esta arquitetura
architecture tabela of mem_prog is
begin  -- tabela

  instr <= program( BV2INT7(ender) );

end tabela;

