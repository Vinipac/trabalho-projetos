-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
-- UFPR, BCC, ci210 2017-2 trabalho semestral, autor: Roberto Hexsel, 21out
-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++


-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
-- processador MICO XI
-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
library ieee; use ieee.std_logic_1164.all;
use IEEE.numeric_std.all;
use work.p_wires.all;


entity mico is
  port (rst,clk : in bit);
end mico;

architecture functional of mico is

  component mem_prog is                 -- no arquivo mem.vhdss
    port (ender : in  reg7;
          instr : out reg32);
  end component mem_prog;

  component display is                  -- neste arquivo
    port (rst,clk : in bit;
          enable  : in bit;
          data    : in reg32);
  end component display;

  component ULA is                      -- neste arquivo
    port (fun : in reg4;
          alfa,beta : in  reg32;
          gama      : out reg32);
  end component ULA;
 
  component R is                        -- neste arquivo
    port (clk         : in  bit;
          wr_en       : in  bit;
          r_a,r_b,r_c : in  reg4;
          A,B         : out reg32;
          C           : in  reg32);
  end component R;

  component RAM is                      -- neste arquivo
    port (rst, clk : in  bit;
          sel      : in  bit;           -- ativo em 1
          wr       : in  bit;           -- ativo em 1
          ender    : in  reg16;         -- endereço
          data_inp : in  reg32;
          data_out : out reg32);
  end component RAM;

  component regIP is
    port (
    IP_input: in reg16;
    IP_output: out reg16;
    clk: in bit
    );
  end component regIP;

  component muxIP is
    port (
    IPsomado: in reg16;
    const: in reg16;
    A: in reg16;
    selNextIP: in reg2;
    NextIP: out reg16
    ) ;
  end component muxIP;

  component somaIP is
    port (
    IP: in reg16;
    IPsomado: out reg16
    ) ;
  end component somaIP;

  component mux_selbeta is
    port(B: in  reg32;
         const: in reg16;
         sel_beta   : in  bit;
         beta   : out reg32);
  end component mux_selbeta;

  component muxC is
    port (
    IPsomado: in reg16;
    gama: in reg32;
    data_out: in reg32;
    selC: in reg2;
    C: out reg32
    ) ;
  end component muxC;


  type t_control_type is record
    selNxtIP   : reg2;     -- seleciona fonte do incremento do IP
    selC       : reg2;     -- seleciona fonte da escrita no reg destino
    wr_reg     : bit;      -- atualiza banco de registradores
    selBeta    : bit;      -- seleciona fonte para entrada B da ULA
    mem_sel    : bit;      -- habilita acesso a RAM
    mem_wr     : bit;      -- habilita escrita na RAM
    wr_display : bit;      -- atualiza display=1
  end record;

  type t_control_mem is array (0 to 15) of t_control_type;
  
  constant ctrl_table : t_control_mem := (
  --sNxtIP selC  wrR selB  Msel Mwr wrDsp
    ("00", "00", '0', '0', '0', '0', '0'),            -- NOP
    ("00", "01", '1', '0', '0', '0', '0'),            -- ADD
    ("00", "01", '1', '0', '0', '0', '0'),            -- SUB
    ("00", "01", '1', '0', '0', '0', '0'),            -- MUL
    ("00", "01", '1', '0', '0', '0', '0'),            -- AND
    ("00", "01", '1', '0', '0', '0', '0'),            -- OR
    ("00", "01", '1', '0', '0', '0', '0'),            -- XOR
    ("00", "01", '1', '0', '0', '0', '0'),            -- NOT
    ("00", "01", '1', '1', '0', '0', '0'),            -- ADDI
    ("00", "10", '1', '0', '1', '0', '0'),            -- LD
    ("00", "00", '0', '1', '0', '1', '0'),            -- ST
    ("00", "00", '0', '0', '0', '0', '1'),            -- SHOW
    ("01", "00", '1', '1', '0', '0', '0'),            -- JAL (tomar cuidado. "c" é r15)
    ("10", "00", '0', '0', '0', '0', '0'),            -- JR
    ("00", "00", '0', '0', '0', '0', '0'),            -- BRANCH
    ("00", "00", '0', '0', '0', '0', '0'));           -- HALT

  constant HALT : bit_vector := x"f";

  signal igual: bit;
  signal selNxtIP, selNxtIPwithBranch, selC : reg2;
  signal selBeta, wr_display, wr_reg : bit;
  signal mem_sel, mem_wr : bit;

  signal instr, A, B, C, beta, gama, extended, ula_D, mem_D, data_out : reg32;
  signal this  : t_control_type;
  signal const, ip, next_ip, ext_zeros, ext_sinal : reg16;
  signal opcode, reg_a, reg_b, reg_c : reg4;
  signal i_opcode : natural range 0 to 15;
  signal ip_somado : reg16;
  signal int_a, int_b : integer;
  
begin  -- functional


  -- memoria de programa contem somente 128 palavras


  opcode <= instr(31 downto 28);
  reg_a  <= instr (27 downto 24);
  reg_b  <= instr (23 downto 20);
  reg_c  <= instr (19 downto 16);
  
  i_opcode <= BV2INT4(opcode);          -- indice do vetor DEVE ser inteiro
  const    <= instr(15 downto 0);
  
  this <= ctrl_table(i_opcode);         -- sinais de controle

  selBeta    <= this.selBeta;
  wr_display <= this.wr_display;
  selNxtIP   <= this.selNxtIP;
  wr_reg     <= this.wr_reg;
  selC       <= this.selC;
  mem_sel    <= this.mem_sel;
  mem_wr     <= this.mem_wr;


  U_mem_prog: mem_prog port map(ip(6 downto 0), instr);
  U_ip_add:   somaIP   port map(ip(15 downto 0), ip_somado(15 downto 0));
  U_ip_mux:   muxIP    port map(ip_somado, const, A(15 downto 0), selNxtIP, next_ip);
  U_ip_reg:   regIP    port map(next_ip, ip, clk);

  U_regs: R port map (clk, wr_reg, reg_a, reg_b, reg_c, A, B, C);

  U_mux_selbeta: mux_selbeta port map (B, const, selBeta, beta);
  
  U_ULA: ULA port map ( opcode, A, beta, gama);

  U_mux_selC: muxC port map (ip_somado, gama, data_out, selC, C);

  U_mem: RAM port map (rst, clk, mem_sel, mem_wr, gama(15 downto 0), B, data_out);

  comparador : process( A, B )
  begin
    
    int_a <= BV2INT(A);
    int_b <= BV2INT(B);

    if (int_a = int_b) then
      igual <= '1';
    else
      igual <= '0'; 
    end if ;
  end process comparador; 

  muxBranch: process(igual, opcode, selNxtIP)
  begin
    if ((igual='1') and (opcode = x"e")) then
      selNxtIPwithBranch <= b"01";
    else
      selNxtIPwithBranch <= selNxtIPwithBranch;
    end if;
  end process muxBranch;


  -- nao altere esta linha
  U_display: display port map (rst, clk, wr_display, A);

  
  assert opcode /= HALT
    report LF & LF & "simulation halted: " & 
    "ender = "&integer'image(BV2INT16(ip))&" = "&BV16HEX(ip)&LF
    severity failure;
  
end functional;
-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++




-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
use work.p_wires.all;

entity ULA is
  port (fun : in reg4;
        alfa,beta : in  reg32;
        gama      : out reg32);
end ULA;

architecture behaviour of ULA is

  component adderCSA32 is port(inpA, inpB : in reg32;
                           outC : out reg32;
                           vem : in bit;
                           vai  : out bit);
  end component adderCSA32;

  component mult16x16 is port(A, B : in  reg16;   -- entradas A,B
                              prod : out reg32);  -- produto
  end component mult16x16;

  
  signal addResult : reg32;
  signal comp_beta: reg32;
  signal subResult : reg32;
  signal multResult: reg32;
  signal overflow1: bit; -- não leva a lugar nenhum
  signal overflow2: bit; -- não leva a lugar nenhum

begin  -- behaviour

U_add:  adderCSA32 port map (alfa, beta, addResult, '0', overflow1);
U_sub:  adderCSA32 port map (alfa, comp_beta, subResult, '1', overflow2);
U_mult: mult16x16 port map (alfa(15 downto 0), beta(15 downto 0), multResult);

complementa_beta: process(beta)
begin
  comp_beta(15 downto 0) <= not(beta(15 downto 0));
end process complementa_beta;

resp: process (fun, alfa, beta, addResult, subResult, multResult)
begin
  
  case fun is 
  
    when x"1"   =>   gama <= addResult;
    when x"2"   =>   gama <= subResult;
    when x"3"   =>   gama <= multResult;
    when x"4"   =>   gama <= alfa and beta;
    when x"5"   =>   gama <= alfa or beta;
    when x"6"   =>   gama <= alfa xor beta;

    when x"7"   => gama(15) <= not(alfa(15));
                   gama(14) <= not(alfa(14));
                   gama(13) <= not(alfa(13));
                   gama(12) <= not(alfa(12));
                   gama(11) <= not(alfa(11));
                   gama(10) <= not(alfa(10));
                   gama(9) <= not(alfa(9));
                   gama(8) <= not(alfa(8));
                   gama(7) <= not(alfa(7));
                   gama(6) <= not(alfa(6));
                   gama(5) <= not(alfa(5));
                   gama(4) <= not(alfa(4));
                   gama(3) <= not(alfa(3));
                   gama(2) <= not(alfa(2));
                   gama(1) <= not(alfa(1));
                   gama(0) <= not(alfa(0));

    when x"8"   =>   gama <= addResult;

    when others =>   gama <= x"00000000";

  end case;


end process;

end behaviour;
-- -----------------------------------------------------------------------



-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
-- display: exibe inteiro na saida padrao do simulador
--          NAO ALTERE ESTE MODELO
-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
library IEEE; use std.textio.all;
use work.p_wires.all;

entity display is
  port (rst,clk : in bit;
        enable  : in bit;
        data    : in reg32);
end display;

architecture functional of display is
  file output : text open write_mode is "STD_OUTPUT";
begin  -- functional

  U_WRITE_OUT: process(clk)
    variable msg : line;
  begin
    if falling_edge(clk) and enable = '1' then
      write ( msg, string'(BV32HEX(data)) );
      writeline( output, msg );
    end if;
  end process U_WRITE_OUT;

end functional;
-- ++ display ++++++++++++++++++++++++++++++++++++++++++++++++++++++++



-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
-- memoria RAM, com capacidade de 64K palavras de 32 bits
--          NAO ALTERE ESTE MODELO
-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
library ieee; use ieee.std_logic_1164.all;
use IEEE.numeric_std.all;
use work.p_wires.all;

entity RAM is
  port (rst, clk : in  bit;
        sel      : in  bit;          -- ativo em 1
        wr       : in  bit;          -- ativo em 1
        ender    : in  reg16;
        data_inp : in  reg32;
        data_out : out reg32);

  constant DATA_MEM_SZ : natural := 2**16;
  constant DATA_ADDRS_BITS : natural := log2_ceil(DATA_MEM_SZ);

end RAM;

architecture rtl of RAM is
  
  subtype t_address is unsigned((DATA_ADDRS_BITS - 1) downto 0);
  
  subtype word is bit_vector(31 downto 0);
  type storage_array is
    array (natural range 0 to (DATA_MEM_SZ - 1)) of word;
  signal storage : storage_array;
begin
  
  accessRAM: process(rst, clk, sel, wr, ender, data_inp)
    variable u_addr : t_address;
    variable index, latched : natural;

    variable d : reg32 := (others => '0');
    variable val, i : integer;

  begin

    if (rst = '0') and (sel = '1') then -- normal operation

      index := BV2INT16(ender);

      if  (wr = '1') and rising_edge(clk) then
        
        assert (index >= 0) and (index < DATA_MEM_SZ)
          report "ramWR index out of bounds: " & natural'image(index)
          severity failure;

        storage(index) <= data_inp;
        
        assert TRUE report "ramWR["& natural'image(index) &"] "
          & BV32HEX(data_inp); -- DEBUG
        
      else

        assert (index >= 0) and (index < DATA_MEM_SZ)
          report "ramRD index out of bounds: " & natural'image(index)
          severity failure;

        d := storage(index);
        
        assert TRUE report "ramRD["& natural'image(index) &"] "
          & BV32HEX(d);  -- DEBUG

      end if; -- normal operation

      data_out <= d;

    else

      data_out <= (others=>'0');

    end if; -- is reset?
    
  end process accessRAM; -- ---------------------------------------------
  
end rtl;
-- -----------------------------------------------------------------------



-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
-- banco de registradores
--          NAO ALTERE ESTE MODELO
-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
use work.p_wires.all;

entity R is
  port (clk         : in  bit;
        wr_en       : in  bit;          -- ativo em 1
        r_a,r_b,r_c : in  reg4;
        A,B         : out reg32;
        C           : in  reg32);
end R;

architecture rtl of R is
  type reg_file is array(0 to 15) of reg32;
  signal reg_file_A : reg_file;
  signal reg_file_B : reg_file;
  signal int_ra, int_rb, int_rc : integer range 0 to 15;
begin

  int_ra <= BV2INT4(r_a);
  int_rb <= BV2INT4(r_b);
  int_rc <= BV2INT4(r_c);

  A <= reg_file_A( int_ra ) when r_a /= b"0000" else
       x"00000000";                        -- reg0 always zero
  B <= reg_file_B( int_rb ) when r_b /= b"0000" else
       x"00000000";

  WRITE_REG_BANKS: process(clk)
  begin
    if rising_edge(clk) then
      if wr_en = '1' and r_c /= b"0000" then
        reg_file_A( int_rc ) <= C;
        reg_file_B( int_rc ) <= C;
      end if;
    end if;
  end process WRITE_REG_BANKS;
  
end rtl;
--------------------------------------------------------------------------

-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
--                       COMPONENTES DO PROCESSADOR
-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
-- Mux SelBeta
-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
-- escolhe const quando sel_beta esta em 1 e B quando esta em 0

use work.p_wires.all;

entity mux_selbeta is
  port(B: in  reg32;
       const: in reg16;
       sel_beta   : in  bit;
       beta   : out reg32);
end mux_selbeta;

architecture functional of mux_selbeta is

signal sign_ext : reg32;

begin

  signal_extend : process( const, sign_ext )
  begin
    case const(15) is
      when '0'    => sign_ext <= x"0000" & const(15 downto 0); -- concatena 16 zeros
      when '1'    => sign_ext <= x"FFFF" & const(15 downto 0); -- concatena 16 uns
      when others => sign_ext <= x"00000000"; -- retorna 32 zeros
    end case;
  end process ; -- signal_extend

  mux : process( B, sel_beta )
  begin
    case sel_beta is
      when '1'    => beta <= sign_ext;
      when others => beta <= B;
    end case;
  end process ; -- mux

end functional ; -- functional

-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
-- somaIP
-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
use work.p_wires.all;

entity somaIP is
  port (
  IP: in reg16;
  IPsomado: out reg16
  ) ;
end somaIP;

architecture functional of SomaIP is
  
  component adderAdianta16 is
    port(inpA, inpB : in reg16;
         outC : out reg16;
         vem  : in bit;             -- '0' soma, '1' subtrai    
         vai  : out bit
         );
    end component adderAdianta16;

      signal overflow: bit; -- não utilizado

begin

  U_add: adderAdianta16 port map (IP, x"0001", IPsomado, '0', overflow);

  end functional ; -- functional

-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
-- muxIP
-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
use work.p_wires.all;

entity muxIP is
  port (
  IPsomado: in reg16;
  const: in reg16;
  A: in reg16;
  selNextIP: in reg2;
  NextIP: out reg16
  ) ;
end muxIP;

architecture functional of muxIP is

begin
  muxIP: process(IPsomado, const, A, selNextIP)
  begin
    case (selNextIP) is
     when b"00" => NextIP <= IPsomado;
     when b"01" => NextIP <= const;
     when b"10" => NextIP <= A;
     when others => NextIP <= x"ffff"; -- nao deve acontecer, existe só em caso de bugs
    end case;

  end process ; -- functional

end;

-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
-- regIP
-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
use work.p_wires.all;

entity regIP is
  port (
  IP_input: in reg16;
  IP_output: out reg16;
  clk: in bit
  );
end regIP;

architecture functional of regIP is

begin

  regIP: process (IP_input, clk)
  begin
    if rising_edge(clk) then
     IP_output <= IP_input;
    end if;
  end process;

end functional ; -- functional

-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
-- muxC
-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
use work.p_wires.all;

entity muxC is
  port (
  IPsomado: in reg16;
  gama: in reg32;
  data_out: in reg32;
  selC: in reg2;
  C: out reg32
  ) ;
end muxC;

architecture functional of muxC is

  signal ip32 : reg32;

begin

sign_ext: process(IPsomado)
    begin
      case IPsomado(15) is
       when '0'    => ip32 <= x"0000" & IPsomado(15 downto 0); -- concatena 16 zeros
       when '1'    => ip32 <= x"FFFF" & IPsomado(15 downto 0); -- concatena 16 uns
       when others => ip32 <= x"00000000"; -- retorna 32 zeros
      end case;
    end process sign_ext;

mux: process(ip32, gama, data_out, selC )
  begin
    case selC is
      when b"00" => C <= ip32;
      when b"01" => C <= gama;
      when b"10" => C <= data_out;
      when others => C <= ip32; -- nao deve acontecer, existe só em caso de bugs
    end case;
  end process mux; 


end functional;


